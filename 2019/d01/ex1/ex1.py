#!/usr/bin/env python
import sys


def main() -> None:
    print(sum(int(l) // 3 - 2 for l in sys.stdin))


if __name__ == "__main__":
    main()
